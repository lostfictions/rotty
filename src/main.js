import rot from "rot-js";

const display = new rot.Display({
  width:20,
  height:15,
  fontFamily: 'NotoEmoji',
  fontSize: 25,
  forceSquareRatio: true
});

document.querySelector('#container').appendChild(display.getContainer());


display.draw(5, 4, "🐖", "pink");
display.draw(15, 4, "%", "#0f0");          /* foreground color */
display.draw(18, 4, "#", "#f00", "#009");  /* and background color */

// const map = {};
//
// function initMap() {
//   const digger = new rot.Map.Digger();
//
//   const digCallback = (x, y, value) => {
//     if(value) { return; } /* do not store walls */
//
//     const key = x+","+y;
//     map[key] = ".";
//   };
//   digger.create(digCallback);
// }
//
// function drawMap() {
//   for(const key in map) {
//     const parts = key.split(",");
//     const x = parseInt(parts[0], 10);
//     const y = parseInt(parts[1], 10);
//     display.draw(x, y, map[key]);
//   }
// }
//
//
// initMap();
// drawMap();
//
//
// const scheduler = new rot.Scheduler.Simple();
// const engine = new rot.Engine(scheduler);
// const output = [];
//
// /* sample actor: pauses the execution when dead */
// const actor = {
//   lives: 3,
//   act: function() {
//     let done = null;
//     const promise = {
//       then: function(cb) { done = cb; }
//     };
//
//     output.push(".");
//     this.lives--;
//
//     /* if alive, wait for 500ms for next turn */
//     if (this.lives) {
//       setTimeout(function() { done(); }, 500);
//     }
//
//     return promise;
//   }
// };
//
// scheduler.add(actor, true);
//
// engine.start();

